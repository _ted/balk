""" Representations of various baseball happenings. """

import logging
from dataclasses import dataclass, field
from typing import List

def _pitch_type(pitch_code: str) -> str:
    """
    Translate MLB's pitch codes to something easier to read.
    see https://www.mlb.com/glossary/pitch-types
    """
    code_map = {
            "CH": "CHNG",
            "CU": "CURV",
            "EP": "EEPH",
            "FA": "4SFB",
            "FC": "CUT",
            "FF": "4SFB",   # glossary says FA, but we're getting FF
            "FO": "FORK",
            "FS": "SPLT",
            "FT": "2SFB",
            "KC": "KNCV",
            "KN": "KNUC",
            "SC": "SCRW",
            "SI": "SINK",
            "SL": "SLID",
            "ST": "SWPR",
            "SV": "SLRV",
    }
    if pitch_code not in code_map:
        logging.warn(f"unknown pitch code {pitch_code}")
    return code_map.get(pitch_code, "????")


class Event:
    """ Marker supertype. """
    pass

@dataclass
class Pitch(Event):
    """
    Represents one pitch and its outcome.
    """
    detailed_description: str = ""
    pitch_code: str = ""
    pitch_speed: float = 0.0
    hit_speed: float = 0.0
    is_in_play: bool = False
    is_fouled: bool = False
    is_strike: bool = False
    is_ball: bool = False

    def short_str(self) -> str:
        pitch_type = _pitch_type(self.pitch_code)

        result = "???"
        if self.is_in_play:
            result = "in play"
        elif self.is_fouled:
            result = "foul"
        elif self.is_strike:
            result = "strike"
        elif self.is_ball:
            result = "ball"

        return f"{pitch_type:<4} {self.pitch_speed:>5} {result}"


@dataclass
class NoPitch(Event):
    """
    Represents a call of no pitch.
    """
    event: str = "????"

    def short_str(self) -> str:
        match self.event:
            case "VP":
                return "clockvio pitcher"
            # why are there so many codes for this
            case "AC" | "CA" | "VB":
                return "clockvio batter"
            case _: return self.event


@dataclass
class Balk(Event):
    """
    The one true outcome.
    """
    def short_str(self) -> str:
        return "balk!"


@dataclass
class BatterTimeout(Event):
    """
    Messing with the pitcher?
    """
    def short_str(self) -> str:
        return "batter timeout"


@dataclass
class DisengagementViolation(Event):
    """
    You don't get unlimited pickoff attempts anymore.
    """
    def short_str(self) -> str:
        return "disengage vio"


@dataclass
class MoundVisit(Event):
    """
    Stalling for time?
    """
    def short_str(self) -> str:
        return "mound visit"


@dataclass
class PassedBall(Event):
    """
    When the catcher does an oopsie.
    """
    def short_str(self) -> str:
        return "passed ball"


@dataclass
class WildPitch(Event):
    """
    When the pitcher does an oopsie.
    """
    def short_str(self) -> str:
        return "wild pitch"


@dataclass
class Play:
    """
    Represents one play, which is the outcome of a plate appearance or another
    event of similar importance.
    """

    description: str = ""
    # unused?
    detailed_description: str = ""
    event: str = ""
    eventType: str = ""
    time: str = ""
    sequence: List[Pitch] = field(default_factory=list)

    def __str__(self):
        return f"{self.description}\n\n"
