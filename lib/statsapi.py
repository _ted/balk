import datetime
import json
import logging
import requests
import sys

import dateutil.parser

from collections import defaultdict
from datetime import timedelta
from typing import Dict, List, Optional, Tuple, Union

from .events import *
from .types import FieldState, Game, GameState, Team


# APIs documented at https://statsapi.mlb.com/docs/endpoints/$endpoint

class Stats():
    _statsapi_domain = "http://statsapi.mlb.com"
    _schedule_resource = \
            "/api/v1/schedule?sportId=1&date={0}&hydrate=team,linescore"
    _game_resource = "/api/v1/game/{0}/feed/live"
    _play_by_play_resource = "/api/v1/game/{0}/playByPlay"
    _max_lookback_days = 7

    @staticmethod
    def url(resource: str) -> str:
        return Stats._statsapi_domain + resource

    def _get(self, resource: str) -> Dict:
        url = Stats.url(resource)
        try:
            request = requests.get(url)
        except Exception as e:
            logging.exception(f"GET {url} failed: {e}")
            return None
        if not request.status_code == 200:
            logging.warn(f"GET {url} failed with "
                         f"status_code={request.status_code}, "
                         f"body={request.text}")
            return None
        try:
            return json.loads(request.text)
        except json.decoder.JSONDecodeError:
            logging.exception(
                f"GET {url} returned invalid json, body={request.text}")
            return None

    def _get_schedule(self, date: datetime.date) -> Dict:
        date_str = date.strftime("%Y-%m-%d")
        schedule_resource = Stats._schedule_resource.format(date_str)
        logging.info(f"retrieving schedule from {Stats.url(schedule_resource)}")
        schedule = self._get(schedule_resource)
        return schedule

    def get_games(self, schedule: Dict, date: datetime.date) -> List[Game]:
        dates = schedule.get("dates", [])
        date_str = date.strftime("%Y-%m-%d")
        matching_dates = [d for d in dates if d["date"] == date_str]
        if not matching_dates:
            logging.info(f"no data for {date_str}")
            return []

        games = [
            self.parse_game_from_schedule(g) for g in matching_dates[0]["games"]
        ]
        return [g for g in games if g]

    def get_latest_games(self) -> Optional[Tuple[List[Game], datetime.date]]:
        next_date = datetime.date.today()

        for _ in range(0, Stats._max_lookback_days + 1):
            schedule = self._get_schedule(next_date)
            if not schedule:
                logging.error("couldn't get schedule, we should probably "
                              "implement retries")
                sys.exit(1)
            games = self.get_games(schedule, next_date)

            if games:
                logging.info(f"returning {len(games)} games for {next_date}")
                return games, next_date

            next_date -= timedelta(days=1)

        logging.info("exhausted lookback and found no games")
        return None

    def parse_game_from_schedule(self, state) -> Optional[Game]:
        if "teams" in state:
            teams = state["teams"]
        else:
            return None

        if state.get("gameDate"):
            start_time = dateutil.parser.isoparse(
                state["gameDate"]).astimezone()
        else:
            return None

        home_team = teams.get("home")
        away_team = teams.get("away")
        city = home_team.get("team", {}).get("locationName")
        stadium = state.get("venue", {}).get("name")

        if "team" in home_team and away_team:
            if "abbreviation" in home_team["team"] and away_team["team"]:
                home_team = Team[teams["home"]["team"]["abbreviation"]]
                away_team = Team[teams["away"]["team"]["abbreviation"]]
            else:
                return None
        else:
            return None

        game_id = state.get("gamePk")
        details_url: str = state.get("link")

        game = Game(start_time, home_team, away_team, game_id,
                stadium, city, details_url)

        game_code = state["status"]["abstractGameCode"]
        detailed_state = state["status"].get("detailedState", "")
        game.game_state = self._game_state(game_code, detailed_state)

        if "linescore" in state:
            linescore = state["linescore"]

            if "runs" in linescore["teams"]["away"] and \
                    "runs" in linescore["teams"]["home"]:
                game.home_team_total_runs = linescore["teams"]["home"]["runs"]
                game.away_team_total_runs = linescore["teams"]["away"]["runs"]

            if "innings" in linescore:
                inning_state = linescore["innings"]

                if inning_state:
                    game.current_inning = linescore["currentInning"]
                    game.is_top_of_inning = linescore["isTopInning"]

        return game

    # TODO: better error handling for null data
    def update_game_from_details(self, game: Game) -> None:
        logging.info(f"updating {game} from {Stats.url(game.details_url)}")
        detail_data = self._get(game.details_url)
        if not detail_data:
            logging.warn(f"failed to update game data for {game}")
            return

        live_data = detail_data["liveData"]["linescore"]
        if not live_data["innings"]:
            return

        game_data = detail_data.get("gameData", {})
        if "detailedState" in game_data["status"] and \
            game_data["status"]["detailedState"].startswith("Suspended"):
            game_code = "D"
        else:
            game_code = game_data["status"]["abstractGameCode"]
        game_code = game_data["status"]["abstractGameCode"]
        detailed_state = game_data.get("status", {}).get("detailedState", "")
        game.game_state = self._game_state(game_code, detailed_state)

        game.current_inning = live_data.get("currentInning", None)
        game.is_top_of_inning = live_data.get(
            "inningHalf", "top").lower() == "top"
        game.away_team_total_runs = live_data["teams"]["away"].get("runs", "")
        game.away_team_total_hits = live_data["teams"]["away"].get("hits", "")
        game.away_team_total_errors = live_data["teams"]["away"].get(
            "errors", 0)
        game.home_team_total_runs = live_data["teams"]["home"].get("runs", "")
        game.home_team_total_hits = live_data["teams"]["home"].get("hits", "")
        game.home_team_total_errors = live_data["teams"]["home"].get(
            "errors", 0)

        def last_name(player: Dict) -> str:
            return " ".join(player["fullName"].split()[1:])

        def last_name_and_initial(player: Dict) -> str:
            names = player["fullName"].split()
            return f"{names[0][0]}. {names[1]}"

        def player_id(player: Dict) -> str:
            return player["id"]

        field_state = FieldState()
        field_state.pitcher = last_name_and_initial(live_data["defense"]["pitcher"])
        field_state.batter = last_name_and_initial(live_data["offense"]["batter"])

        field_state.is_runner_on_first = "first" in live_data["offense"]
        field_state.is_runner_on_second = "second" in live_data["offense"]
        field_state.is_runner_on_third = "third" in live_data["offense"]

        field_state.num_outs = live_data.get("outs", "")
        field_state.num_balls = live_data.get("balls", "")
        field_state.num_strikes = live_data.get("strikes", "")

        box_score = detail_data["liveData"]["boxscore"]
        pitch_counts = {}
        for player in (box_score["teams"]["home"]["players"] | box_score["teams"]["away"]["players"]).values():
            if player.get("position", "").get("name", "").lower() == "pitcher":
                if len(player["stats"]) and "pitching" in player["stats"] and len(player["stats"]["pitching"]):
                    pitch_counts[player["person"]["id"]] = player["stats"]["pitching"].get("pitchesThrown", 0)

        pitcher_id = player_id(live_data["defense"]["pitcher"])
        field_state.pitch_count = pitch_counts[pitcher_id]

        lineup_number = 0
        for index, id in enumerate(box_score["teams"]["home"]["battingOrder"] + box_score["teams"]["away"]["battingOrder"]):
            if id == player_id(live_data["offense"]["batter"]):
                lineup_number = (index % 9) + 1

        field_state.lineup_number = lineup_number

        game.field_state = field_state

        all_box_score_innings = live_data["innings"]

        # 1-index for convenience
        away_team_box_score_innings, home_team_box_score_innings = \
            [None], [None]

        for inning in all_box_score_innings:
            away_team_box_score_innings.append(inning["away"].get("runs", ""))
            home_team_box_score_innings.append(inning["home"].get("runs", ""))

        game.away_team_box_score_innings = away_team_box_score_innings
        game.home_team_box_score_innings = home_team_box_score_innings

        game.plays = self._get_play_by_play(game)


    def _game_state(self, code: str, detailed_state: str):
        if "Delayed" in detailed_state or "Suspended" in detailed_state:
            return GameState.DELAY
        if "Postponed" in detailed_state:
            return GameState.POSTPONED
        return GameState.from_code(code)


    def _get_play_by_play(self, game: Game) -> List[Play]:
        play_by_play_data = self._get(self._play_by_play_resource.format(game.game_id))
        if not play_by_play_data:
            logging.warn(f"failed to get play by play data for {game}, will try again")
            return []

        all_plays = play_by_play_data.get("allPlays")
        if not all_plays:
            return []

        plays: List[Play] = []
        # track the last play so we can detect the end of inning halves
        last_play_data = dict()
        for play_data in all_plays:
            # insert an end-of-half play if this is the start of a new half
            last_half = (
                    last_play_data.get("about", dict()).get("inning", None),
                    last_play_data.get("about", dict()).get("isTopInning", None)
            )
            this_half = (
                    play_data.get("about", dict()).get("inning", None),
                    play_data.get("about", dict()).get("isTopInning", None)
            )
            if this_half != last_half:
                end_half_play = Play()
                half = "Top" if this_half[1] else "Bottom"
                ordinalizer = defaultdict(
                        lambda: "th", ((1, "st"), (2, "nd"), (3, "rd")))
                ordinal = str(this_half[0]) + ordinalizer[this_half[0]]
                end_half_play.description = \
                        f"/^\\ {half} of the {ordinal}. /^\\"
                plays.append(end_half_play)

            play = Play()
            if result := play_data.get("result"):
                if "event" in result:
                    play.description = result.get("description", "")
                    play.description = " ".join(play.description.split())
                    play.event = result.get("event", "")
                    play.eventType = result.get("eventType", "")
                else:
                    batter = play_data.get("matchup", dict()).get("batter",
                            dict()).get("fullName", "??? ???")
                    play.description = f"{batter} at bat..."

            if about := play_data.get("about"):
                play.time = about.get("endTime", "")

            if play_events := play_data.get("playEvents"):
                for event in play_events:
                    match result := _extract_play_or_event(
                            event, about, game):
                        case Play():
                            plays.append(result)
                        case Event():
                            play.sequence.append(result)

            plays.append(play)
            last_play_data = play_data

        return plays

def _is_ignored_event(event_type: Optional[str], detailed_event_type:
                      Optional[str]) -> bool:
    if event_type == "stepoff": return True
    if detailed_event_type:
        if "game_advisory" in detailed_event_type: return True
    return False


def _extract_play_or_event(event: dict, about: dict, game: Game
                           ) -> Optional[Union[Play, Event]]:
    """about and game passed for debugging"""
    event_type = event.get("type")
    details = event.get("details", {})
    detailed_event_type = details.get("eventType")
    description = details.get("description", "????")
    if _is_ignored_event(event_type, detailed_event_type): return

    match event_type:
        case "pitch":
            pitch = Pitch()
            pitch.pitch_speed = event.get("pitchData", dict()).get("startSpeed", 0.0)
            pitch.hit_speed = event.get("hitData", dict()).get("launchSpeed", 0.0)
            pitch.pitch_code = details.get("type", dict()).get("code", "??")
            pitch.detailed_description = details.get("call", dict()).get("description", "")
            pitch.is_in_play = details.get("isInPlay", False)
            pitch.is_fouled = details.get("description") == "Foul"
            pitch.is_strike = details.get("isStrike", False)
            pitch.is_ball = details.get("isBall", False)
            return pitch
        case "no_pitch":
            no_pitch = NoPitch()
            no_pitch.event = details.get("call", {}).get("code", "????")
            if no_pitch.event == "????":
                # Possibly, balks are represented as two events:
                # no_pitch with no details + actually a balk
                logging.error(
                    "no pitch, but unknown event code, "
                    f"game={game}, about={about}, event={event}")
            return no_pitch
        case "action":
            match detailed_event_type:
                case "balk":
                    return Balk()
                case "batter_timeout":
                    return BatterTimeout()
                case "mound_visit":
                    return MoundVisit()
                case "passed_ball":
                    return PassedBall()
                case "wild_pitch":
                    return WildPitch()
                case "forced_balk":
                    if details.get("violation", {}).get("type") == "pitcher_disengagement":
                        return DisengagementViolation()

    if description:
        return Play(description)
    else:
        logging.error(f"unhandled event: {event}")
