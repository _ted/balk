""" Pull current conditions for games from yr.no. """

import json
import logging
import math
from typing import Optional

import requests

from .types import Weather

_INDOOR_STADIUMS = {
        "American Family Field",
        "Chase Field",
        "Minute Maid Park",
}

_LOCATION_ID_CACHE = {}

def _get_location_id(location_name) -> Optional[str]:
    """ Attempt to retrieve a yr.no ID for the given location. """
    url = "https://www.yr.no/api/v0/locations/search"\
         f"?language=en&q={location_name}"
    logging.info(url)
    try:
        logging.info(f"retrieving location ID for {location_name}")
        r = requests.get(url)
        if not r.status_code == 200:
            logging.warn(f"got a {r.status_code} trying to retrieve location "
                    f"id for {location_name} from {url}")
            return None
        response = json.loads(r.text)
    except Exception as e:
        logging.exception("something went wrong retrieving location id for "
                f"{location_name}, e={e}")
        return None

    results = response.get("_embedded", {}).get("location", [])
    def squash(s):
        # reduce s to just its letters in lowercase
        return "".join([c for c in s.lower() if ord("a") <= ord(c) <= ord("z")])
    target = squash(location_name)
    for result in results:
        if squash(result.get("name", "")) == target:
            location_id = result.get("id")
            path = result.get("urlPath")
            logging.info(f"resolved {location_name} to {location_id} ({path})")
            return location_id
    logging.info(f"couldn't find location id for {location_name} from {url}")
    return None

def has_weather(stadium_name: str) -> bool:
    """ Returns whether stadium is affected by weather. """
    return stadium_name not in _INDOOR_STADIUMS

def current_weather(location_name: str) -> Optional[Weather]:
    global _LOCATION_ID_CACHE
    if location_name not in _LOCATION_ID_CACHE:
        location_id = _get_location_id(location_name)
        if not location_id:
            logging.warn(f"skipping weather for {location_name} since we "
                    "couldn't get a location id")
            return None
        _LOCATION_ID_CACHE[location_name] = location_id

    location_id = _LOCATION_ID_CACHE.get(str(location_name))
    if not location_id:
        logging.warn(f"unknown location for {location_name}")
        return None

    url = (f"https://www.yr.no/api/v0/locations/{location_id}"
            "/forecast/currenthour")
    try:
        logging.info(f"retrieving weather for {location_name} from {url}")
        r = requests.get(url)
        if not r.status_code == 200:
            logging.warn(f"got a {r.status_code} trying to retrieve weather "
                    f"for {location_name}, from {url}")
            return None
        wjson = json.loads(r.text)
    except Exception as e:
        logging.exception("something went wrong retrieving weather for "
                f"{location_name}, e={e}")
        return None

    wind_direction = wjson.get("wind", {}).get("direction")
    if wind_direction is not None:
        # yr.no encodes wind direction as degrees clockwise of south. Convert to
        # 8 slices of 45° each. Advance by half a slice so N can start from 0
        # and the math gets simpler.
        direction_i = math.floor((wind_direction + 22.5) / 45) % 8
        wind_arrow = ("↓", "↙", "←", "↖", "↑", "↗", "→", "↘")[direction_i]
        logging.debug(
                f"converted wind_direction={wind_direction} into {wind_arrow}")
        wind_direction = wind_arrow

    weather = Weather(condition=wjson.get("symbolCode", {}).get("next1Hour"),
                      temp_c=wjson.get("temperature", {}).get("value"),
                      feelslike_c=wjson.get("temperature", {})
                                         .get("feelsLike"),
                      expected_precipitation=wjson.get("precipitation", {})
                                                    .get("value"),
                      wind_speed_metric=wjson.get("wind", {}).get("speed"),
                      wind_direction=wind_direction)
    logging.info(f"{location_name} has weather={weather}")
    return weather
